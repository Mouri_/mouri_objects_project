function pairs(obj){
    let pairsArray = [];
    for (const keyAndValue of Object.entries(obj)) {
        pairsArray.push(keyAndValue);
    }
    return pairsArray;
}
module.exports = pairs;