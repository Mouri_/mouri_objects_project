function keys(obj) {
    let resultArray = [];
    for (const key in obj) {
        resultArray.push(key);
    }
    return resultArray;
}
module.exports = keys;