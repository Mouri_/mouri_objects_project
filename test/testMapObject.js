let mapObject = require('../mapObject')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham'};
let resultObj = mapObject(testObject, (value)=> `${value} is modified`);    // Callback functions concats string " is modified" for test purposes
console.log(JSON.stringify(resultObj));