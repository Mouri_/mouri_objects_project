function mapObject(obj, callback){
    let mappedObject = {};
    for (const key in obj) {
        mappedObject[key] = callback(obj[key]);
    }
    return mappedObject;
}
module.exports = mapObject;