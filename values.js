function values(obj){
    let arr = [];
    for (const key in obj) {
        let objectItem = obj[key];
        if(typeof objectItem !== 'function'){
            arr.push(obj[key]);
        }
    }
    return arr;
}
module.exports = values;